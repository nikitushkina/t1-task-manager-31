package ru.t1.nikitushkina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.service.IAuthService;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;
import ru.t1.nikitushkina.tm.api.service.IUserService;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.exception.user.*;
import ru.t1.nikitushkina.tm.model.User;
import ru.t1.nikitushkina.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @Nullable
    private String userId;

    public AuthService(
            @NotNull IPropertyService propertyService,
            @NotNull IUserService userService
    ) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) {
        if (roles == null) return;
        @Nullable final User user = getUser();
        @Nullable final Role role = user.getRole();
        if (role == null) throw new PermissionException();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new PermissionException();
    }

    @NotNull
    @Override
    public User getUser() {
        if (!isAuth()) throw new AccessDeniedException();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new AccessDeniedException();
        return user;
    }

    @Override
    public String getUserId() {
        if (!isAuth()) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final boolean locked = user.isLocked() == null || user.isLocked();
        if (locked) throw new UserLockedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (!hash.equals((user.getPasswordHash()))) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @NotNull
    @Override
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        return userService.create(login, password, email);
    }

}
