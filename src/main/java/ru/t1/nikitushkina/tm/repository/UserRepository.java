package ru.t1.nikitushkina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.repository.IUserRepository;
import ru.t1.nikitushkina.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        return models
                .stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        return models
                .stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) {
        return models
                .stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) {
        return models
                .stream()
                .anyMatch(m -> email.equals(m.getEmail()));
    }

}
