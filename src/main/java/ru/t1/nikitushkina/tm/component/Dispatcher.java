package ru.t1.nikitushkina.tm.component;

import org.jetbrains.annotations.NotNull;

import java.util.LinkedHashMap;
import java.util.Map;

import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.api.endpoint.Operation;
import ru.t1.nikitushkina.tm.dto.request.AbstractRequest;
import ru.t1.nikitushkina.tm.dto.response.AbstractResponse;

public class Dispatcher {

    @NotNull
    private final Map<Class<? extends AbstractRequest>, Operation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull final Class<RQ> reqClass,
            @NotNull final Operation<RQ, RS> operation
    ) {
        map.put(reqClass, operation);
    }

    @NotNull
    public Object call(@NotNull final AbstractRequest request) {
        @Nullable final Operation operation = map.get(request.getClass());
        return operation.execute(request);
    }

}
